import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/*
 * This class is designed to be a java equivalent of the tstGenerator.rb file originally written for
 * android intents guitar. This class implements parsing via the DOM model which presents the documents as
 * a tree based structure. The second half of the original tstGenerator.rb file implemented here 
 */

public class GenerateTst {
	
	private static String sequenceFilePath;

	/**
	 * @param args
	 *            Author: Ely, Adrian, Jean
	 * 
	 */
	public static void main(String argv[]) {
		
		if(argv.length == 0 || argv[0] == null || argv[0].length() == 0){
			System.err.println("File path for sequence.txt not provided as an arg");
			System.exit(-1);
			return;
		}
			
		sequenceFilePath= argv[0];
		
		
		
		
		try {
			/*
			 * the hash map that maps a string to an array list of hash maps is the data structure used to hold mappings from one activity to another via button IDs.
			 * this construct is preferred because simply using a hash map will overwrite values for the same key, if we have to map the same key to multiple values. This 
			 * structure will grow a list of hash maps for the same key if there a re multiple mappings to one key.
			 * 
			 */
			HashMap<String, ArrayList<HashMap<String, String>>> h_map = new HashMap<String, ArrayList<HashMap<String, String>>>();

			File fXmlFile = new File("Demo.GUI");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

		

			NodeList guiList = doc.getElementsByTagName("GUI");

			String name = null;
			String id = null;
			String invokeList = null;

			for (int i = 0; i < guiList.getLength(); i++) {
				Node gui = guiList.item(i);
				
				NodeList guiChildern = gui.getChildNodes();
				for (int j = 0; j < guiChildern.getLength(); j++) {
					Node m = guiChildern.item(j);

					if (m.getNodeName().equals("Window")) {
						

						Node attributes = m.getFirstChild().getNextSibling();
						NodeList propertyList = attributes.getChildNodes();

						for (int i2 = 0; i2 < propertyList.getLength(); i2++) {
							Node property = propertyList.item(i2);
							if (property.getNodeType() != Node.TEXT_NODE) {
								

								HashMap<String, String> map = getNodeMap(property);

								if (map.containsValue("Title")) {
									
									name = map.get("Value");
									break;
								}
							}
						}

					}
					if (m.getNodeName().equals("Container")) {

						ArrayList<Node> widgetList = new ArrayList<Node>();
						
						getChildNodeListByNameRec(m, "Widget", widgetList);

						for (Node widgetNode : widgetList) {
							
							ArrayList<Node> propertyList = new ArrayList<Node>();
							getChildNodeListByNameRec(widgetNode, "Property",
									propertyList);

							

							for (Node PropertyNode : propertyList) {

								HashMap<String, String> map = getNodeMap(PropertyNode);

								if (map.containsValue("ID")) {
									
									id = map.get("Value");
									id = id.replaceAll("w", "e");

								}
								if (map.containsValue("Invokelist")) {
									
									invokeList = map.get("Value");
									
									/*
									 * put the Name, invokelist, and id to a
									 * hashmap---------That's what we need for
									 * this class!
									 * 
									 * 
									 * we save all result of (Title, InvokeList,
									 * ID) to a HashMap<String,
									 * List<HashMap<String, String>>>
									 * ..........|
									 * .....|.............|..........\.....
									 * ......
									 * ....name..ListOfHashMap.(InvokeList,ID)..
									 */
									if (!h_map.containsKey(name)) {
										HashMap<String, String> hp = new HashMap<String, String>();
										hp.put(invokeList, id);
										ArrayList<HashMap<String, String>> lst = new ArrayList<HashMap<String, String>>();
										lst.add(hp);
										h_map.put(name, lst);
									} else {
										HashMap<String, String> hp = new HashMap<String, String>();
										hp.put(invokeList, id);
										h_map.get(name).add(hp);
									}

									break;
								}

							}

						}
					}

				}
			}

			/*
			 * Example of iterating hash map :
			 * http://stackoverflow.com/questions
			 * /1066589/java-iterate-through-hashmap
			 * 
			 * we save all result of (Title, InvokeList, ID) to a
			 * HashMap<String, List<HashMap<String, String>>>
			 * ..........|.....|.............|..........\.....
			 * ...........Title.ListOfHashMap.(InvokeList,ID)..
			 * 
			 * following code is for testing the output whether correct or not
			 */

			Iterator<Entry<String, ArrayList<HashMap<String, String>>>> it = h_map
					.entrySet().iterator();
			while (it.hasNext()) {
				@SuppressWarnings("rawtypes")
				Map.Entry pairs = (Map.Entry) it.next();

				System.out.println(pairs.getKey() + " = " + pairs.getValue());

				// it.remove(); // avoids a ConcurrentModificationException
			}

			/*
			 * 
			 * 
			 * 
			 * 
			 * Start using sequence.txt to generate testcase
			 */

			/*
			 * # generates test cases based on sequence of intents
			 * 
			 * #a = Hash.new endSequence = false #concatenate into testcase name
			 * eg: testcase_1.tst testcasename = 1 #array of already existed
			 * testcases tstString = Array.new File.open(ARGV[0], "r").each{
			 * |line| #has steps / sequence hasStep = false #initializing the
			 * testcase string testcase =
			 * "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<TestCase>"
			 * if(line.include?"Sequence 1" || endSequence == true) endSequence
			 * = true break end
			 * 
			 * while (line.include?"/") line = line.sub(/\//, "") end
			 * 
			 * #puts "line: '#{line}'"
			 * 
			 * line = line.strip arrSequence = line.split(" --> ")
			 */

			// Open the file
			
			FileInputStream fstream = new FileInputStream(sequenceFilePath);

			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			String strLine;
			boolean endSequence = false;
			int testcasename = 1;
			String[] arrSequence = null;
			String testCase = "";
			int ind = 0;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {

				// Print the content on the console
				if (strLine.length() != 0) {
					if (strLine.indexOf("Sequence 1") != -1
							|| endSequence == true) {
						endSequence = true;
						break;
					}
					String eventId = "";

					testCase = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<TestCase>";

					while (strLine.indexOf("/") != -1) {
						strLine = strLine.replaceAll("/", "");
					}

					strLine = strLine.trim();
					arrSequence = strLine.split(" --> ");

					boolean hasStep = false;
					String curr = "";
					String next = "";
					for (int idx = 0; idx < arrSequence.length - 1; idx++) {
						//System.out.println(arrSequence[idx]);
						//System.out.println(arrSequence[idx +1]);
						curr = arrSequence[idx];
						next = arrSequence[idx + 1];

						//System.out.println(h_map.get(curr));
						if (h_map.get(curr)== null){
							continue;
						}
						for (HashMap<String, String> map : h_map.get(curr)) {
							for (Entry<String, String> entry : map.entrySet()) {
								
								//System.out.println(entry.getKey());
								if (entry.getKey().equals(next)) {

									eventId = entry.getValue();
									
									testCase = testCase
											.concat("\n<Step>\n\t<EventId>"
													+ eventId
													+ "</EventId>\n\t<ReachingStep>true</ReachingStep>\n</Step>");
									hasStep = true;
								}
							}
						}

					}

					if (hasStep) {

						testCase = testCase.concat("\n</TestCase>");
					

					System.out.println("testCase " + testCase);
					
					//writing to a tst file for each case
					
					
					ind++;
					
					File file = new File("testcase_"+ ind +".tst");
		 
					// if file doesnt exists, then create it
					if (!file.exists()) {
						file.createNewFile();
					}
		 
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(testCase);
					bw.close();
					}
		
					
					
					
					
					
					System.out.println("------------------------" + ind);

				}
			}

			// Close the input stream
			in.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param ParentNode
	 * @param name
	 * @return Node with the name passes in or null if no Node exists
	 * 
	 * 
	 * 
	 *         private static ArrayList<Node> getChildNodeListByName(Node
	 *         ParentNode, String name) { if (ParentNode == null) { return null;
	 *         } NodeList childernList = ParentNode.getChildNodes(); if
	 *         (childernList == null) { return null; }
	 * 
	 *         ArrayList<Node> nameList = new ArrayList<Node>(); for (int i = 0;
	 *         i < childernList.getLength(); i++) { Node curChild =
	 *         childernList.item(i); if (curChild.getNodeName().equals(name)) {
	 *         nameList.add(curChild); } }
	 * 
	 *         return nameList; }
	 */

	private static ArrayList<Node> getChildNodeListByNameRec(Node ParentNode,
			String name, ArrayList<Node> list) {
		if (ParentNode == null) {
			return null;
		}
		NodeList childernList = ParentNode.getChildNodes();
		if (childernList == null) {
			return null;
		}
		for (int i = 0; i < childernList.getLength(); i++) {
			Node curChild = childernList.item(i);
			if (curChild.hasChildNodes()) {
				getChildNodeListByNameRec(curChild, name, list);
			}
			if (curChild.getNodeName().equals(name)) {
				list.add(curChild);
			}
		}

		return list;
	}

	private static HashMap<String, String> getNodeMap(Node ParentNode) {
		HashMap<String, String> map = new HashMap<String, String>();

		if (ParentNode == null) {
			return null;
		}
		NodeList childernList = ParentNode.getChildNodes();
		if (childernList == null) {
			return null;
		}
		for (int i = 0; i < childernList.getLength(); i++) {
			Node curChild = childernList.item(i);

			map.put(curChild.getNodeName(), curChild.getTextContent());
		}

		return map;
	}

}