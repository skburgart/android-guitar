package tcc.testing;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.umd.cs.guitar.model.XMLHandler;
import edu.umd.cs.guitar.model.data.AttributesType;
import edu.umd.cs.guitar.model.data.EFG;
import edu.umd.cs.guitar.model.data.EventType;
import edu.umd.cs.guitar.model.data.PropertyType;

public class OptionalTest {

	@Test
	public void test() {
		
		//read in EFG with optional data and ensure it can be extracted
		
		XMLHandler xml = new XMLHandler();
		
		String path = "/home/cluster/.jenkins/workspace/WebGuitarJobGIT/WebGuitar/WebGuitarManualTesting/Input/152aaed4.EFG";
		
		EFG efg = (EFG) xml.readObjFromFile(path, EFG.class);
		
		
		for(EventType event : efg.getEvents().getEvent()){
			
			AttributesType opt = event.getOptional();
			
			if(opt != null){
				
				for(PropertyType prop : opt.getProperty()){
					
					System.out.println(prop.getName() + " => " + prop.getValue());
					
				}
			}
			
		}
		
		
		
	}

}
