from __future__ import division
from sets import Set
import re
import sys

def get_classes(apiFileName):
    classes = Set();
    with open(apiFileName) as f:
        for line in f:
            match = re.search(r'(?:invoke|override): (.*)->.*', line)
            if match:
                 classes.add(match.group(1))
    return classes

def get_logcat_matches(locatFileName, classes):
    with open(logcatFileName) as f:
        content = f.readlines()

    matched = Set();
    for line in content:
        for c in classes:
            if line.find(c) != -1 and c not in matched:
                matched.add(c)
    return matched
    
if __name__ == "__main__":

    apiFileName = sys.argv[1];
    logcatFileName = sys.argv[2];

    classes = get_classes(apiFileName)
    matched = get_logcat_matches(logcatFileName, classes)

    coveragePercent = int((len(matched) / len(classes)) * 100)
    for c in classes:
	if c in matched:
	    matchstr = "X"
        else:
	    matchstr = " "
        print "[",matchstr,"]", c

    print "\nExecuted", len(matched), "/", len(classes), "API classes"
    print "Class coverage %%%d" % coveragePercent
