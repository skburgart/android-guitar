package simplified.data;

public class SimplifiedGUIElement {

	private String widgetID;
	private int x;
	private int y;
	private String tagName; //referred to as "Title" in GUI
	
	
	
	public SimplifiedGUIElement(String widgetID, String tagName, int x, int y){
		this.widgetID = widgetID;
		this.tagName = tagName;
		this.x = x;
		this.y = y;
		
	}
	
	
	
	public String getWidgetID(){
		return widgetID;
	}
	
	public String getTagName(){
		return tagName;
	}
	
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
}
