package launcher;

import java.util.ArrayList;
import java.util.List;

class AdditionalInfoFile {
	
	

	String timestamp;
	
	String url;

	String filename;
	
	List<String> testcases;
	
	public AdditionalInfoFile(String url, String filename){
		this.url = url;
		this.filename = filename;
		testcases = new ArrayList<String>();
		this.timestamp = Util.getTimestamp();
		
	}
	
	public void addTestCase(String tc){
		testcases.add(tc);
	}
	
}
