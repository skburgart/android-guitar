#!/usr/bin/python

import subprocess
import re
import sys
import os

def getPackageName(apkFile):
    result = subprocess.Popen("aapt dump badging "+apkFile+" | grep package:\ name", shell=True, stdout=subprocess.PIPE).communicate()[0]
    match = re.match(r"package: name=\'(.*?)\'.*", result)
    
    package = ""
    if match:
        package = match.group(1)

    return package

def getActivityName(apkFile):
    result = subprocess.Popen("aapt dump xmltree "+apkFile+" AndroidManifest.xml", shell=True, stdout=subprocess.PIPE).communicate()[0]
    
    match = re.search(r"E:\sactivity.*?A:\s\w+:\w+\(\w+\)\=\"(.*?)\"", result, re.S)

    activity = ""
    if match:
        activity = match.group(1).strip(".")

    return activity


if __name__ == "__main__":
    apkFile = sys.argv[1]
    #apkFile = "~/Dropbox/Schoolwork/CMSC737/APKs/GamblerSMS/f1d8b11012df9b898ca2f9b0a5a97ef79b8a5e1d.apk"
    if len(sys.argv) != 3:
        print "Usage: getAPKInfo.py [apk file] [package/activity]"
        sys.exit(1)
    if sys.argv[2] == "package":
        package = getPackageName(apkFile)
        print package
    elif sys.argv[2] == "activity":
        activity = getActivityName(apkFile)
        print activity

    '''
    print "Package:", package
    print "Activity:", activity
    
    if len(sys.argv) > 2 and sys.argv[2] == "run":
        #os.system("emulator -avd ADRGuitarTest -cpu-delay 0 -noaudio -no-snapshot-save &")
        os.system("adb -e install -r "+apkFile)
        os.system("adb shell am start -n "+package+"/."+activity)
    '''
