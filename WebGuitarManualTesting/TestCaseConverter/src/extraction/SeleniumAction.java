package extraction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public enum SeleniumAction {
	
	CLICK	("click"),
	CLEAR	("clear"),
	TYPE	("sendKeys");
	
	
	
	
	private String methodName;
	
	
	SeleniumAction(String str){
		methodName = str;
		
	}
	
	
	public static void perform(SeleniumAction action,WebElement wE, String param){
		
		switch(action){
		case CLICK:
				wE.click();
				break;
		case CLEAR:
				wE.clear();
				break;
		case TYPE:
				wE.sendKeys(param);
				break;
		
		}
		
		
	}
	
	
	@Override
	public String toString(){
		return methodName;
	}
	
	/**
	 * Dynamic action selector. Uses the ENUM values to fetch Tool objects.
	 * 
	 * 
	 * @param toolName
	 * @return
	 * @throws InvalidSeleniumActionThrowable 
	 * @throws InvalidToolThrowable
	 */
	static public SeleniumAction getAction(String actionName) throws InvalidSeleniumActionThrowable{
		
		if(actionName == null)
			throw new InvalidSeleniumActionThrowable("null is not a valid action!");
		
		
		for(SeleniumAction action : SeleniumAction.values()){
			
			//remove whitespace
			String aName = action.toString();
			
			if(actionName.equalsIgnoreCase(aName)){
				return action;
			}
		}
		
		throw new InvalidSeleniumActionThrowable(actionName + "is not a valid action!");
		
	}

}
