package launcher;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class InfoFileWriter {

	
	
	
	public static void write(AdditionalInfoFile info, String outputpath){
		
		
		
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			
			
			
			Element rootTag = doc.createElement("AdditionalInfo");
			doc.appendChild(rootTag);
			
						
			rootTag.setAttribute("timestamp", info.timestamp);
			
			Element urlTag = doc.createElement("url");
			urlTag.setTextContent(info.url);
			
			rootTag.appendChild(urlTag);
			
			
			Element nameTag = doc.createElement("naming-scheme");
			nameTag.setTextContent(info.filename);
			
			rootTag.appendChild(nameTag);
			
			
			Element testcases = doc.createElement("testcases");
			
			for(String tc : info.testcases){
				Element testCase = doc.createElement("selenium-testcase");
				testCase.setAttribute("name", tc);
				testcases.appendChild(testCase);
				
			}
			
			rootTag.appendChild(testcases);
			
			Util.transform(outputpath,doc);
			
			
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	
}
